#!/usr/bin/env bash

chmod +x run.sh
chmod +x update.sh

docker-compose up -d
docker exec -it documents_app bin/console documents:install
docker-compose down