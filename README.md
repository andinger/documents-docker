# Documents

## Install

Clone this repo

    git clone https://bitbucket.org/andinger/documents-docker.git
    
Run installer script    

    chmod +x install.sh
    ./install.sh
    
Open Browser and open http://localhost:8080

## Change HTTP-Port?

Change Port in _docker-compose.yml_ file
    
## Run

    ./run.sh 
    # or
    docker-compose up -d
    
## Update

    ./update.sh

## Errors?

If you get some errors like "index no found", "schema does not exist ...", run the following command after running the stack

    docker exec -it documents_app bin/console documents:install

## Bugs? Features? Feedback?

https://bitbucket.org/andinger/documents/issues

## Docker Compose

Documents is provided as docker container. You can either create the necessary Docker-Containers for Redis, 
Elasticsearch and MySQL by yourself or run the whole stack via _docker-compose_.

## Inbox-Directory

The inbox directory will be crawled every minute. New files are sent to index and moved into the data-Directory. 
Only OCRd PDFs are supported

